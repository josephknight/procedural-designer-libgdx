package com.josephknight.proceduraldesigner.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.josephknight.proceduraldesigner.ProceduralDesigner;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 600;
		config.x = 1440-config.width;
		config.y = 0;
		config.initialBackgroundColor.set(0x5D5D5Dff);
		new LwjglApplication(new ProceduralDesigner(), config);
	}
}
