package com.josephknight.proceduraldesigner.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.SnapshotArray;
import com.josephknight.proceduraldesigner.ArtWindow;
import com.josephknight.proceduraldesigner.Shape;
import com.josephknight.proceduraldesigner.Tool;
import com.josephknight.proceduraldesigner.Vertex;

public class ToolCreateShape extends Tool {
	
	private InputListener	artWindowBehavior;
	private InputListener	vertexBehavior;					//TODO move listeners to abstract class set method
	private Shape			drawnShape;
	protected boolean		justCompletedAShape	= false;
	private ArtWindow		artWindow;
	
	/** This tool should create a shape without interfering with existing shapes.
	 * 
	 * @param artWindow */
	public ToolCreateShape(ArtWindow theArtWindow) {
		this.artWindow = theArtWindow;
		
		artWindowBehavior = new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if ( justCompletedAShape ) {
					justCompletedAShape = false;
					return true;
				}
				if ( drawnShape == null )
					startNewShape();
				Vertex newVertex = Pools.obtain(Vertex.class);
				newVertex.setPosition(x, y);
				drawnShape.addActor(newVertex);
				return true;
			}
			
		};
		
		vertexBehavior = new InputListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Gdx.app.log(null, "createshape vertex touchdown");
				if ( !(event.getListenerActor() instanceof Vertex) )
					return false;
				if ( drawnShape == null )
					return false;
				Vertex vertex = (Vertex)event.getListenerActor();
				Vertex firstVertex = (Vertex)drawnShape.getChildren().get(0);
				if ( vertex == firstVertex ) {
					completeShape();
					return true;
				} else
					return false;
			}
		};
	}
	
	@Override
	public InputListener getArtWindowBehavior() {//TODO: make these not required to be overidden
		return artWindowBehavior;
	}
	
	@Override
	public InputListener getVertexBehavior() {
		return vertexBehavior;
	}
	
	@Override
	public void draw() {
		//TODO: preview line: draw a selected line between last new vertex and cursor
		//TODO: move drawn shape code here
	}
	
	private void startNewShape() {
		drawnShape = Pools.obtain(Shape.class);
		drawnShape.setColor(artWindow.getFillColor());
		artWindow.addActor(drawnShape);
	}
	
	private void cancelShape() {
		if ( drawnShape == null )
			return;
		drawnShape.remove();
		SnapshotArray<Actor> children = drawnShape.getChildren();
		Actor[] actors = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Vertex vertex = (Vertex)actors[i];
			vertex.remove();
		}
		children.end();
		drawnShape = null;
	}
	
	private void completeShape() {
		if ( drawnShape == null )
			return;
		if ( drawnShape.getChildren().size < 3 )
			cancelShape();
		drawnShape = null;
		justCompletedAShape = true;
	}
	
	public String getToolboxName() {
		return "Create Shape";
	}
}
