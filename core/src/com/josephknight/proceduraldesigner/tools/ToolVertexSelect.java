package com.josephknight.proceduraldesigner.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.UIUtils;
import com.josephknight.proceduraldesigner.ArtWindow;
import com.josephknight.proceduraldesigner.Tool;
import com.josephknight.proceduraldesigner.Vertex;

public class ToolVertexSelect extends Tool {
	private InputListener	vertexBehavior;
	private InputListener	artWindowBehavior;
	private Vector2			touchDownPoint		= new Vector2();
	private Vector2			tempPoint			= new Vector2();
	private final float		dragDeadzoneRadius	= 5.0f;
	protected boolean		dragging			= false;
	protected Vertex		touchedVertex;
	protected boolean		boxSelecting;
	protected Vector2		boxStart			= new Vector2();
	protected Vector2		boxSize				= new Vector2();
	private ArtWindow		artWindow;
	
	public ToolVertexSelect(ArtWindow theArtWindow) {
		this.artWindow = theArtWindow;
		
		vertexBehavior = new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//				Gdx.app.log(null, "vertex touchDown x:" + x + ", y:" + y);
				dragging = false;
				touchedVertex = (Vertex)event.getListenerActor();
				if ( UIUtils.shift() ) {
					touchedVertex.setSelected(!touchedVertex.isSelected());
				} else if ( UIUtils.alt() ) {
					Gdx.app.log(null, "temp ignored alt, would be copy");
				} else if ( !touchedVertex.isSelected() ) {
					artWindow.deselectAll();
					touchedVertex.setSelected(true);
				}
				return true;
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//				Gdx.app.log(null, "vertex tool vertex touchup");
				dragging = false;
			}
		};
		
		artWindowBehavior = new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//				Gdx.app.log(null, "artwindow touchdown x:" + x + ", y:" + y);
				if ( pointer != 0 )
					return false;
				dragging = false;
				touchDownPoint.set(x, y);
				return true;
			}
			
			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
//				Gdx.app.log(null, "artwindow dragging x:" + x + ", y:" + y);
				if ( dragging && touchedVertex != null && touchedVertex.isSelected() ) {
					Vertex[] selection = artWindow.getSelectedVertices();
					float xOffset = x - touchedVertex.getX();
					float yOffset = y - touchedVertex.getY();
					for (Vertex vertex : selection) {
						vertex.setPosition(vertex.getX() + xOffset, vertex.getY() + yOffset);
					}
				} else if ( dragging && touchedVertex == null ) {
					//we didn't start dragging on a vertex so we are boxselecting
					boxSelecting = true;
					boxStart.set(touchDownPoint.x, touchDownPoint.y);
					boxSize.set(x - touchDownPoint.x, y - touchDownPoint.y);
				} else {
					tempPoint.set(x, y);
					if ( tempPoint.dst(touchDownPoint) > dragDeadzoneRadius )
						dragging = true;
				}
			}
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				dragging = false;
				if ( touchedVertex == null && !UIUtils.shift() )
					artWindow.deselectAll();
				if ( boxSelecting ) {
					boxSelecting = false;
					if ( !UIUtils.shift() ) {
						artWindow.deselectAll();
					}
					Vertex[] vertices = artWindow.getAllVertices();
					float bx = boxStart.x;
					float by = boxStart.y;
					float bbx = bx + boxSize.x;
					float bby = by + boxSize.y;
					float[] vertFloats = { bx, by, bx, bby, bbx, bby, bbx, by };
					Polygon box = new Polygon(vertFloats);
					for (Vertex vertex : vertices) {
						if ( box.contains(vertex.getX(), vertex.getY()) )
							vertex.setSelected(true);
					}
				}
				touchedVertex = null;
			}
		};
	}
	
	@Override
	public void draw() {
		if ( boxSelecting ) {
			ShapeRenderer sr = artWindow.getShapeRenderer();
			sr.begin(ShapeType.Line);
			sr.setColor(Color.BLUE);
			sr.rect(boxStart.x, boxStart.y, boxSize.x, boxSize.y);
			sr.end();
		}
	}
	
	@Override
	public InputListener getArtWindowBehavior() {
		return artWindowBehavior;
	}
	
	@Override
	public InputListener getVertexBehavior() {
		return vertexBehavior;
	}
	
	public String getToolboxName() {
		return "Vertex Select";
	}
}
