package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Payload;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Source;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop.Target;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.TimeUtils;
import com.josephknight.proceduraldesigner.Modifier.ModifierChangeListener;

public class StackPanel extends SplitPane implements ModifierChangeListener {
	
	private static final float	MODIFIER_BAR_HEIGHT	= 30.0f;
	private StackApplication	app;
	private ModifierBar			selectedModifierBar;
	private final ScrollPane	detailScrollPane;
	private final VerticalGroup	stackVerticalGroup;
	private final Table			detailTable;
	private final Table			parametersTable;
	private final Label			detailLabel;
	private final ClickListener	modifierBarClickListener;
	private final DragAndDrop	dnd;
	private final Image			sortIndicator;
	private final Table			upperTable;
	private final Vector2		tempCoords			= new Vector2();
	private boolean				previewAll;
	private Skin				skin;
	private ScrollPane			stackScrollPane;						//TEMP field for modbar glitch
	private boolean				dirty;
	private long				lastTime;
	private long				interval			= 5;
	private ArtWindow			artWindow;
	
	public StackPanel(StackApplication app, Skin skin) {
		super(null, null, true, skin);
		ParameterBar.skin = skin;
		this.app = app;
		if ( app.getArtWindow() != null )
			this.artWindow = app.getArtWindow();
		else
			throw new RuntimeException("App does not yet have an artwindow upon StackPanel intialization.");
		this.skin = skin;
		/*Drag and Drop stuff */
		dnd = new DragAndDrop();
		dnd.setKeepWithinStage(false);
		//dnd.setCancelTouchFocus(cancelTouchFocus);//TODO for scrollpane conflict avoidance. R docs.
		sortIndicator = new Image(skin.getDrawable("sort-indicator"));
		sortIndicator.setVisible(false);
		sortIndicator.setTouchable(Touchable.disabled);
		modifierBarClickListener = new ClickListener() {
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				super.touchUp(event, x, y, pointer, button);
				ModifierBar modifierBar;
				if ( event.getTarget() instanceof ModifierBar )
					modifierBar = (ModifierBar)event.getTarget();
				else
					return;
				select(modifierBar);
			}
		};
		
		/* Panes */
		stackVerticalGroup = new VerticalGroup();
		stackVerticalGroup.fill();
		Table stackToolbarTable = new Table(skin).background("panel-bg");
		stackScrollPane = new ScrollPane(stackVerticalGroup, skin);
		stackScrollPane.setFadeScrollBars(false);
//		stackScrollPane.setupFadeScrollBars(1.0f, 8.0f);
//		stackScrollPane.setFlickScroll(false);
//		stackScrollPane.setFadeScrollBars(false);
		
		/* Stack Toolbar Buttons */
		ImageButton previewAllButton = new ImageButton(
				skin.getDrawable("icon-stack-preview-all-OFF"),
				skin.getDrawable("icon-stack-preview-all-ON"),
				skin.getDrawable("icon-stack-preview-all-ON")
				);
		previewAllButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				ImageButton btn = (ImageButton)actor;
				previewAll = btn.isChecked();
				select(selectedModifierBar);
			}
		});
		ImageButton autoCollapseButton = new ImageButton(
				skin.getDrawable("icon-stack-auto-collapse-OFF"),
				skin.getDrawable("icon-stack-auto-collapse-ON"),
				skin.getDrawable("icon-stack-auto-collapse-ON")
				);
		ImageButton collapseSelectedButton = new ImageButton(
				skin.getDrawable("icon-stack-collapse"));
		ImageButton collapseAllButton = new ImageButton(
				skin.getDrawable("icon-stack-collapse-all"));
		ImageButton reselectButton = new ImageButton(
				skin.getDrawable("icon-stack-reselect"));
		reselectButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
//				selectedModifierBar.getModifier().setSelection(???);
			}
		});
		ImageButton makeScriptedButton = new ImageButton(
				skin.getDrawable("icon-stack-make-scripted"));
		ImageButton deleteButton = new ImageButton(
				skin.getDrawable("icon-stack-delete"));
		/* Stack Toolbar Button Functionality */
		deleteButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				removeSelectedModifier();
			}
		});
		
		stackToolbarTable.row().expandX();
		stackToolbarTable.add(previewAllButton);
		stackToolbarTable.add(autoCollapseButton);
		stackToolbarTable.add(collapseSelectedButton);
		stackToolbarTable.add(collapseAllButton);
		stackToolbarTable.add(reselectButton);
		stackToolbarTable.add(makeScriptedButton);
		stackToolbarTable.add(deleteButton);
		
		/* Upper Stack Pane */
		upperTable = new Table(skin).background("tiled-dark-linen");
		upperTable.add(stackScrollPane).expand().fillX().top();
		upperTable.row();
		upperTable.add(stackToolbarTable).height(MODIFIER_BAR_HEIGHT).expandX().fillX();
		upperTable.addActor(sortIndicator);
		
		/* Detail Setup */
		detailLabel = new Label("", skin, "colorable");
		detailLabel.setColor(skin.getColor("text-normal-selected"));
//		detailLabel.setEllipsis(true);
		detailLabel.setAlignment(Align.left);
		detailTable = new Table(skin);
		detailScrollPane = new ScrollPane(null, skin);
		detailScrollPane.setFadeScrollBars(false);
		detailScrollPane.setFlickScroll(false);
		detailScrollPane.setWidget(detailTable);
		detailTable.top().left();
		detailTable.defaults().left().fillX().expandX();
		/* Detail Heading (which modifier is selected) */
		Table detailHeadingTable = new Table();
		detailHeadingTable.defaults().left();
		detailHeadingTable.setBackground(skin.getDrawable("panel-bg-selected"));
		detailHeadingTable.add(new ImageButton(skin, "double-arrow-left-large"));
		detailHeadingTable.add(detailLabel).spaceLeft(8).fillX().expandX();
		detailTable.add(detailHeadingTable);
		detailTable.row();
		parametersTable = new Table();
		detailTable.add(parametersTable);
		
		/* This Stack Pane */
		this.setFirstWidget(upperTable);
		this.setSecondWidget(detailScrollPane);
		
		updateDetailTable();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
	}
	
	/** Pushes a new modifier onto the stack but not necessarily at the top. Will
	 * push onto currently selected modifier and under all those above it.
	 * 
	 * @param modifierClass */
	public void push(Modifier modifier) {
		addModifierAt(modifier, 0);
	}
	
	public void addModifierBeforeSelected(Modifier modifier) {
		addModifierAt(modifier, indexOf(getSelectedModifier()));
	}
	
	public void addModifierAt(Modifier modifier, int index) {
		ModifierBar newModifierBar = BarFactory.createModifierBar(modifier, skin);
		newModifierBar.addListener(modifierBarClickListener);
		newModifierBar.getModifier().setModifierChangeListener(this);
		newModifierBar.getModifier().setSelection(artWindow.getSelectedVertices());
		if ( getSelectedModifier() == null || index == -1 )
			stackVerticalGroup.addActorAt(0, newModifierBar);
		else
			stackVerticalGroup.addActorAt(index, newModifierBar);
		newModifierBar.setStackPanel(this);
		dnd.addSource(new ModifierBarDragSortSource(newModifierBar));
		dnd.addTarget(new ModifierBarDragSortTarget(newModifierBar));
		select(newModifierBar);
		dirty = true;
	}
	
	/** Pops topmost Modifier off the stack and returns it for reference.
	 * 
	 * @return */
	public Modifier pop() {
		SnapshotArray<Actor> modifierBars = stackVerticalGroup.getChildren();
		if ( modifierBars.size == 0 )
			return null;
		//get last modifier (topmost)
		ModifierBar topBar = (ModifierBar)modifierBars.get(modifierBars.size - 1);
		if ( selectedModifierBar == topBar )
			selectNext();
		Modifier returnModifier = topBar.getModifier();
		remove(returnModifier);
		return returnModifier;
	}
	
	public void removeSelectedModifier() {
		if ( selectedModifierBar != null )
			remove(selectedModifierBar.getModifier());
	}
	
	/** Looks for the provided modifier and pops it out wherever it may be.
	 * 
	 * @param modifier
	 * @return true if it was found and removed */
	public boolean remove(Modifier modifier) {
		SnapshotArray<Actor> children = stackVerticalGroup.getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			ModifierBar modifierBar = (ModifierBar)array[i];
			if ( modifierBar.getModifier() == modifier ) {
				selectNext();
				modifierBar.remove();
				Pools.free(modifierBar);
				if ( indexOf(selectedModifierBar) == -1 )
					selectBottommost();
				process();
				return true;
			}
		}
		children.end();
		dirty = true;
		return false;
	}
	
	//method UNTESTED
	/** removes all modifiers from stack **/
	public void clearStack() {
		dnd.clear();
		stackVerticalGroup.clear();
	}
	
	/** @return returns null if nothing selected or empty */
	public Modifier getSelectedModifier() {
		if ( selectedModifierBar == null )
			return null;
		return selectedModifierBar.getModifier();
	}
	
	public boolean select(ModifierBar modifierBarToSelect) {
		int selectIndex = indexOf(modifierBarToSelect);
		if ( selectIndex == -1 ) {
			selectNone();
			return false;
		}
		SnapshotArray<Actor> modifierBars = stackVerticalGroup.getChildren();
		Actor[] array = modifierBars.begin();
		for (int i = 0, n = modifierBars.size; i < n; i++) {
			ModifierBar modifierBar = (ModifierBar)array[i];
			if ( i == selectIndex ) {
				modifierBar.setSelected(true);
				selectedModifierBar = modifierBar;
			} else
				modifierBar.setSelected(false);
			if ( !previewAll && i < selectIndex )
				modifierBar.setDisabled(true);
			else
				modifierBar.setDisabled(false);
		}
		modifierBars.end();
		//TODO: window-select modifier's selection
		//update detail pane
		updateDetailTable();
		requestProcessing();
		return true;
	}
	
	//TODO: selectPrevious()
	private void selectNext() {
		int nextIndex = indexOf(selectedModifierBar) + 1;
		SnapshotArray<Actor> items = stackVerticalGroup.getChildren();
		if ( items.size == 0 ){
			selectNone();
			return;
		}
		Actor next;
		if ( nextIndex >= items.size )
			next = items.get(items.size - 1);
		else
			next = items.get(nextIndex);
		select((ModifierBar)next);
	}
	
	private void selectBottommost() {
		SnapshotArray<Actor> items = stackVerticalGroup.getChildren();
		if ( items.size == 0 ){
			selectNone();
			return;
		}
		ModifierBar mb = (ModifierBar)(items.get(items.size - 1));
		select(mb);
	}
	
	private void selectNone() {
		selectedModifierBar = null;
		detailTable.setVisible(false);
		enableAll();
		requestProcessing();
	}
	
	/** processes modifiers based on preview all state. */
	public void process() {
		if ( !dirty )
			return;
		long thisTime = TimeUtils.millis();
		if ( thisTime - lastTime < interval )
			return;
		lastTime = thisTime;
		processUpToIndex(0);
	}
	
	private void processUpToIndex(int index) {
		if ( index == -1 )
			index = 0;//process all instead
		app.stackPanelProcessBegin();
		//process in reverse order, bottom up.
		SnapshotArray<Actor> children = stackVerticalGroup.getChildren();
		Actor[] actors = children.begin();
		for (int i = children.size - 1; i >= index; i--) {
			ModifierBar modifierBar = (ModifierBar)(actors[i]);
			if ( !modifierBar.isProcessingDisabled() && !modifierBar.isDisabled() )
				modifierBar.getModifier().process();
		}
		children.end();
		app.stackPanelProcessEnd();
		dirty = false;
	}
	
	private int indexOf(ModifierBar modifierBar) {
		if ( modifierBar == null )
			return -1;
		return stackVerticalGroup.getChildren().indexOf(modifierBar, true);
	}
	
	private int indexOf(Modifier modifier) {
		if ( modifier == null )
			return -1;
		SnapshotArray<Actor> children = stackVerticalGroup.getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			ModifierBar modifierBar = (ModifierBar)array[i];
			if ( modifier == modifierBar.getModifier() )
				return stackVerticalGroup.getChildren().indexOf(modifierBar, true);
		}
		children.end();
		return -1;
	}
	
	public long getInterval() {
		return interval;
	}
	
	/** The interval in which to process a stack (in millis)
	 * should the stack be consistently dirty, eg. if a parameter
	 * was being dragged constantly.
	 * Will still only processes if stack is also dirty.
	 * 
	 * @param interval */
	public void setInterval(long interval) {
		this.interval = interval;
	}
	
	/** sets disabled state entire stack modifier bars. Not to be confused
	 * with setting their processing to be disabled.
	 * 
	 * @param disable */
	public void setStackDisabled(boolean disable) {
		SnapshotArray<Actor> children = stackVerticalGroup.getChildren();
		Actor[] actors = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			ModifierBar modBar = (ModifierBar)actors[i];
			modBar.setDisabled(disable);
		}
		children.end();
	}
	
	@Override
	public void setBounds(float x, float y, float width, float height) {
		super.setBounds(x, y, width, height);
		sortIndicator.setX(0);
		sortIndicator.setWidth(width);
		dnd.setDragActorPosition(-width / 2 / 2, MODIFIER_BAR_HEIGHT / 2);
	}
	
	private void enableAll() {
		//TODO
	}
	
	private void updateDetailTable() {
		if ( selectedModifierBar != null ) {
			//modbar selected, fill plane with its parameters
			detailTable.setVisible(true);
			detailLabel.setText(selectedModifierBar.getModifier().toString());
			parametersTable.reset();
			Array<ParameterBar> paramBars = selectedModifierBar.getParameterBars();
			for (ParameterBar parameterBar : paramBars) {
				parametersTable.row();
				parametersTable.add(parameterBar).expandX().fillX();
			}
		} else {
			detailTable.setVisible(false);
			parametersTable.reset();
		}
	}
	
	private class ModifierBarDragSortSource extends DragAndDrop.Source {
		public ModifierBarDragSortSource(Actor actor) {
			super(actor);
		}
		
		private int	dragSortOriginalIndex;
		
		@Override
		public Payload dragStart(InputEvent event, float x, float y, int pointer) {
			Payload payload = new Payload();
			payload.setObject(getActor());
			payload.setDragActor(getActor());
			//store its current stack index in case the dragsort is cancelled (with esc or drag outside and away)
			ModifierBar sourceModifierBar = (ModifierBar)getActor();
			dragSortOriginalIndex = indexOf(sourceModifierBar);
			//set semi transparent
			sourceModifierBar.setColor(1.0f, 1.0f, 1.0f, 0.3f);
			return payload;
		}
		
		@Override
		public void dragStop(InputEvent event, float x, float y, int pointer, Payload payload, Target target) {
			Gdx.app.log("drag stop of source", getActor().getName());
			//if drop was not valid, replace in original stack index position
			ModifierBar sourceModifierBar = (ModifierBar)getActor();
			if ( target == null ) {
				stackVerticalGroup.addActorAt(dragSortOriginalIndex, sourceModifierBar);
			}
			//restore transparency
			sourceModifierBar.setColor(Color.WHITE);
			sortIndicator.setVisible(false);
		}
	}
	
	private class ModifierBarDragSortTarget extends DragAndDrop.Target {
		
		public ModifierBarDragSortTarget(Actor actor) {
			super(actor);
		}
		
		@Override
		public boolean drag(Source source, Payload payload, float x, float y, int pointer) {
			Gdx.app.log("drag over target", getActor().getName());
			ModifierBar targetModifierBar = (ModifierBar)getActor();
			ModifierBar payloadModifierBar = (ModifierBar)payload.getObject();
			if ( targetModifierBar == payloadModifierBar )//happens when dragging outside stage
				return false;
			//show a sorting indicator either above or below the modbar we're hovering over
			sortIndicator.setVisible(true);
			if ( y >= targetModifierBar.getHeight() / 2 ) {
				tempCoords.set(0, targetModifierBar.getHeight() - sortIndicator.getHeight() / 2 - 1);
				targetModifierBar.localToAscendantCoordinates(upperTable, tempCoords);
				sortIndicator.setPosition(tempCoords.x, tempCoords.y);
			}
			else {
				tempCoords.set(0, -sortIndicator.getHeight() / 2 - 1);
				targetModifierBar.localToAscendantCoordinates(upperTable, tempCoords);
				sortIndicator.setPosition(tempCoords.x, tempCoords.y);
			}
			return true;
		}
		
		@Override
		public void drop(Source source, Payload payload, float x, float y, int pointer) {
			if ( !(getActor() instanceof ModifierBar) )
				return;
			if ( !(payload.getObject() instanceof ModifierBar) )
				return;
			ModifierBar targetModifierBar = (ModifierBar)getActor();
			ModifierBar draggedModifierBar = (ModifierBar)payload.getObject();
			if ( y >= targetModifierBar.getHeight() / 2 )
				stackVerticalGroup.addActorBefore(targetModifierBar, draggedModifierBar);
			else
				stackVerticalGroup.addActorAfter(targetModifierBar, draggedModifierBar);
			select(draggedModifierBar);
		}
		
		@Override
		public void reset(Source source, Payload payload) {
			Gdx.app.log("reset from", getActor().getName());
			sortIndicator.setVisible(false);
		}
	}
	
	@Override
	public void modifierChanged(Modifier modifier, Parameter parameter) {
		requestProcessing();
	}
	
	public void requestProcessing() {
		dirty = true;
	}
}
