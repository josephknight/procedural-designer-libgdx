package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Pools;
import com.josephknight.proceduraldesigner.parameters.AngleBar;
import com.josephknight.proceduraldesigner.parameters.AngleParameter;
import com.josephknight.proceduraldesigner.parameters.IntegerBar;
import com.josephknight.proceduraldesigner.parameters.IntegerParameter;
import com.josephknight.proceduraldesigner.parameters.RandomNumberBar;
import com.josephknight.proceduraldesigner.parameters.RandomNumberParameter;
import com.josephknight.proceduraldesigner.parameters.RangeBar;
import com.josephknight.proceduraldesigner.parameters.RangeParameter;

/** Uses pooling to create modifier and parameter UI bars. Created items should
 * be freed when no longer in use.
 * 
 * @author joseph */
public class BarFactory {
	
	public static ModifierBar createModifierBar(Modifier modifier, Skin skin) {
		ModifierBar modifierBar = Pools.obtain(ModifierBar.class);
		//Add modifier specific constructor parameters if needed here in the 
		//factory, not elsewhere.
		modifierBar.initialize(modifier, skin);
		return modifierBar;
	}

	public static ParameterBar createParameterBar(Parameter parameter) {
		if (parameter instanceof RangeParameter)
			return new RangeBar((RangeParameter)parameter);
		else if (parameter instanceof AngleParameter)
			return new AngleBar((AngleParameter)parameter);
		else if (parameter instanceof RandomNumberParameter)
			return new RandomNumberBar((RandomNumberParameter)parameter);
		else if (parameter instanceof IntegerParameter)
			return new IntegerBar((IntegerParameter)parameter);
		else
			throw new RuntimeException("Cannot create bar for parameters of type, "+parameter.getClass().toString());
	}
}
