package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Pool.Poolable;

public class Vertex extends Actor implements Poolable{
	final int				ICON_SIZE	= 10;
	private boolean			selected;
	private Texture			currentTexture;
	private final Texture	textureUnselected;
	private final Texture	textureSelected;
	
	public Vertex(){
		this(0,0);
	}
	
	public Vertex(float x, float y) {
		setBounds(x, y, ICON_SIZE, ICON_SIZE);
		//prepare icon images: hollow and solid little squares
		Pixmap pixmap = new Pixmap(ICON_SIZE, ICON_SIZE, Format.RGBA8888);
		pixmap.setColor(Color.BLUE);
		pixmap.drawRectangle(0, 0, ICON_SIZE, ICON_SIZE);
		textureUnselected = new Texture(pixmap);
		
		pixmap = new Pixmap(ICON_SIZE, ICON_SIZE, Format.RGBA8888);
		pixmap.setColor(Color.BLUE);
		pixmap.fillRectangle(0, 0, ICON_SIZE, ICON_SIZE);
		textureSelected = new Texture(pixmap);
		
		selected = false;
		currentTexture = textureUnselected;
	}
	
	@Override
	public Actor hit(float x, float y, boolean touchable) {
		if ( touchable && this.getTouchable() != Touchable.enabled )
			return null;
		float halfsize = .5f * ICON_SIZE;
		return x >= -halfsize && x < halfsize && y >= -halfsize && y < halfsize? this : null;
	}
	
	@Override
	protected void drawDebugBounds(ShapeRenderer sr) {
		if ( !getDebug() )
			return;
		sr.set(ShapeType.Line);
		sr.setColor(getStage().getDebugColor());
		float halfsize = .5f * ICON_SIZE;
		sr.rect(getX() - halfsize, getY() - halfsize, getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		/* TODO: make sure the artwindow transforms the batch appropriately 
		 * based on pan n zoom so that owned vertices can simply use pos x,y
		 */
		float halfsize = .5f * ICON_SIZE;
		batch.draw(currentTexture, getX() - halfsize, getY() - halfsize);
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean selected) {
		if ( selected == this.selected )
			return;
		this.selected = selected;
		if ( selected )
			currentTexture = textureSelected;
		else currentTexture = textureUnselected;
	}

	@Override
	public void reset() {
		this.remove();
		this.setTouchable(Touchable.enabled);
		setPosition(0, 0);
		setSelected(false);
		clear();
	}
}
