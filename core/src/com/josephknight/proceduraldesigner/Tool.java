/*
 * icon (postponed)
 * cursor (postponed)
 * mouse listener for art window
 */

package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.scenes.scene2d.InputListener;

public abstract class Tool {
	
	/**
	 * Derived from the actual Object.toString() but used by apps to list the 
	 * Tool in the toolbox. Must be implemented to output the desired 
	 * toolbox name in all subclasses.
	 */
	public String toString(){
		return getToolboxName();
	};
	
	public InputListener getArtWindowBehavior() {
		return null;
	};
	
	public InputListener getShapeBehavior() {
		return null;
	};
	
	public InputListener getVertexBehavior() {
		return null;
	};
	
	public abstract void draw();

	public abstract String getToolboxName();
}
