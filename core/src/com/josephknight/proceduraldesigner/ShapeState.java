package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.SnapshotArray;

/** State of Shapes and their Vertices */
public class ShapeState implements Poolable {
	float						x, y;
	private Shape				hostReference;
	private Group				parent;
	private Array<VertexState>	vertexStates	= new Array<VertexState>(VertexState.class);
	
	@Override
	public void reset() {
		hostReference = null;
		vertexStates.clear();
		x = y = 0.0f;
	}
	
	public static ShapeState create(Shape shape) {
		ShapeState state = Pools.obtain(ShapeState.class);
		state.hostReference = shape;
		state.parent = shape.getParent();
		state.x = shape.getX();
		state.y = shape.getY();
		SnapshotArray<Actor> children = shape.getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Vertex vertex = (Vertex)array[i];
			state.vertexStates.add(VertexState.create(vertex));
		}
		children.end();
		return state;
	}
	
	public static void restore(ShapeState state) {
		if ( state.hostReference == null )
			throw new NullPointerException("All Shape states must have a host.");
		Shape host = state.hostReference;
		host.clear();//bold move... may cause issues with future added functionality unless that functionality is preserved across add/removal of vertices
		host.setParent(state.parent);
		host.setX(state.x);
		host.setY(state.y);
		for (VertexState vertexState : state.vertexStates) {
			VertexState.restore(vertexState);
		}
	}
	
	private static class VertexState implements Poolable {
		float			x, y;
		private Vertex	hostReference;
		private Group	parent;
		
		@Override
		public void reset() {
			hostReference = null;
			x = y = 0;
		}
		
		public static VertexState create(Vertex vertex) {
			VertexState state = Pools.obtain(VertexState.class);
			state.hostReference = vertex;
			state.parent = vertex.getParent();
			state.x = vertex.getX();
			state.y = vertex.getY();
			return state;
		}
		
		public static void restore(VertexState state) {
			if ( state.hostReference == null )
				throw new NullPointerException("All Vertex states must have a host.");
			Vertex host = state.hostReference;
			host.clear();
//			host.setParent(state.parent);
			state.parent.addActor(host);
			host.setX(state.x);
			host.setY(state.y);
		}
	}
	
	public Actor getHost() {
		return hostReference;
	}
}
