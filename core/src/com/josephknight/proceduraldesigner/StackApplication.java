package com.josephknight.proceduraldesigner;

public interface StackApplication {

	/**
	 * Called just before the stack processes it's first modifier so that the 
	 * app may prepare by setting the vertices and shapes to their base state.
	 */
	void stackPanelProcessBegin();
	void stackPanelProcessEnd();
	ArtWindow getArtWindow();
	
}
