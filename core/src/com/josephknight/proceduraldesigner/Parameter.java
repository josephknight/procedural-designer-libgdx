package com.josephknight.proceduraldesigner;

import java.security.InvalidParameterException;

import com.badlogic.gdx.Gdx;

public abstract class Parameter {
	
	protected String				name;
	protected float					value;
	private Modifier				parentModifier;
	private ParameterChangeListener	parameterChangeListener;
	private boolean					processingNotifications;
	
	public Parameter(String name, Modifier parentModifier) {
		this.name = name;
		this.parentModifier = parentModifier;
	}
	
	public float getValue() {
		return value;
	}
	
	public void setValue(float value) {
		if ( this.value == value ) {
			Gdx.app.log("Parameter.setValue skipped (same value)", Float.toString(value));
			return;
		}
		Gdx.app.log("Parameter.setValue to " + Float.toString(value), "notifying paramChLstnrs.");
		this.value = value;
		notifyChange();
	}
	
	public void notifyChange() {
		if ( processingNotifications )
			return;
		processingNotifications = true;
		if ( parameterChangeListener != null )
			parameterChangeListener.parameterChanged(this);
		if ( parentModifier != null )
			parentModifier.parameterChanged(this);
		processingNotifications = false;
	}
	
	public Modifier getParentModifier() {
		return parentModifier;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		//TODO consider value class and also allow Value class if implemented
		if ( !(obj instanceof Parameter) ) { throw new InvalidParameterException("Can only compare a Parameter with another Parameter"); }
		Parameter otherParameter = (Parameter)obj;
		return this.value == otherParameter.getValue();
	}
	
	public void setParameterChangeListener(ParameterChangeListener listener) {
		this.parameterChangeListener = listener;
	}
	
	public interface ParameterChangeListener {
		public void parameterChanged(Parameter parameter);
	}
}
