package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.josephknight.proceduraldesigner.Parameter.ParameterChangeListener;

public abstract class ParameterBar extends Table implements ParameterChangeListener {
	protected Parameter	parameter;
	public static Skin	skin;
	protected Label		nameLabel;
	protected TextField	valueField;
	protected boolean	isChangeEventsDisabled;
	
	public ParameterBar(final Parameter parameter) {
		super();
		if ( skin == null )
			throw new IllegalStateException("Static field 'skin' must be set before instatiating a ParameterBar.");
		setSkin(skin);
		nameLabel = new Label("undefined parameter", skin, "colorable");
		nameLabel.setHeight(30.0f);//TODO test whether necessary
		nameLabel.setColor(skin.getColor("text-normal"));
		valueField = new TextField("", skin);
		
		setBackground(skin.getDrawable("panel-bg"));
		defaults().left().pad(4);
		pad(8);
		
		this.parameter = parameter;
		setName(parameter.toString());
		parameter.setParameterChangeListener(this);

		nameLabel.setText(getName());
		valueField.setText(Float.toString(parameter.getValue()));
		valueField.setTextFieldFilter(new TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char c) {
				return Character.isDigit(c) || (c == '.');
			}
		});
		//Set underlying parameter's value upon value text box's change in focus or enter key
		valueField.addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
//				updateParameterFromTextField(parameter, (TextField)actor);
				parameter.setValue(textFieldToFloat((TextField)actor));
			}
		});
		valueField.setTextFieldListener(new TextField.TextFieldListener() {
			@Override
			public void keyTyped(TextField textField, char c) {
				if ( c == '\r' || c == '\n' )
//					updateParameterFromTextField(parameter, textField);
					parameter.setValue(textFieldToFloat(textField));
			}
		});
	}
	
	protected float textFieldToFloat(TextField textField){
		try {
			return Float.parseFloat(textField.getText());
		} catch (NumberFormatException e) {
			return 0.0f;//TODO: consider: is this bad practice?
		}
	}
	
	void setValueFieldIsEditable(boolean isEditable) {
		valueField.setDisabled(!isEditable);
	}
	
	@Override
	public void parameterChanged(Parameter parameter) {
		Gdx.app.log("ParameterBar.parameterChanged", "Setting value text.");
		this.valueField.setText(Float.toString(parameter.getValue()));
		Gdx.graphics.requestRendering();
	}

}
