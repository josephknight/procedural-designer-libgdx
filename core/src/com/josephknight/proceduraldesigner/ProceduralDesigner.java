package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.josephknight.proceduraldesigner.tools.ToolCreateShape;
import com.josephknight.proceduraldesigner.tools.ToolVertexSelect;

/*
 * Color suggestions
 * palette bg: #49
 * art window bg: #5D
 * font white: #DD
 */
public class ProceduralDesigner extends ApplicationAdapter implements ArtWindowApplication, StackApplication {
	private OrthographicCamera	camera;
	private Viewport			viewport;
	private Skin				skin;
	private Stage				stage;
	
	private StackPanel			stackPanel;
	private SplitPane			toolboxPane;
	
	private ArtWindow			artWindow;
	private Tool				activeTool;
	//TEMP temporary test declarations
	private float[]				startingVertices	= { 20, 20, 200, 20, 200, 200 };
	private final int			STACK_WIDTH			= 300;
	private Array<ShapeState>	baseShapeStates		= new Array<ShapeState>(ShapeState.class);
	private DebugMode			debugMode			= DebugMode.NO_DEBUG;
	
	FPSLogger					fps					= new FPSLogger();
	private List<Tool>			toolboxToolList;
	
	@Override
	public void create() {
		Gdx.graphics.setContinuousRendering(false);
		Gdx.graphics.requestRendering();
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		viewport = new ScreenViewport(camera);
		stage = new Stage(viewport);
		stage.setActionsRequestRendering(true);
		skin = new Skin(Gdx.files.internal("pdui.json"));
		artWindow = new ArtWindow(this);
		
		//TEMP temporary test objects
		//Temporary Test shape
		testShape = new Shape(startingVertices, new Color(0xDEADBEFF));
		testShape.setStrokeColor(new Color(0x661166FF));
		testShape.setStrokeWidth(5.0f);
		artWindow.addActor(testShape);//TEMP test shape
		storeArtWindowAsBaseState();//TEMP set initial test base artwindow state
		
		//STACK
		stackPanel = new StackPanel(this, skin);
		
		//TOOLBOX
		//toolbox tools
		toolboxToolList = new List<Tool>(skin);
		Tool toolVertexSelect = new ToolVertexSelect(artWindow);
		toolboxToolList.setItems(toolVertexSelect, new ToolCreateShape(artWindow));
		setActiveTool(toolVertexSelect);
		final ScrollPane toolboxUpperScrollPane = new ScrollPane(toolboxToolList, skin);
		toolboxToolList.addListener(new ChangeListener() {
			/** Changes the tool once selected in the ToolBox */
			@SuppressWarnings("unchecked")
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				setActiveTool(((List<Tool>)actor).getSelected());
			}
		});
		//toolbox modifiers
		final List<ModifierToolboxItems> modifierToolbox = new List<ModifierToolboxItems>(skin);
		modifierToolbox.setItems(ModifierToolboxItems.values());
		modifierToolbox.setSelectedIndex(-1);
		modifierToolbox.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				@SuppressWarnings("unchecked")
				List<ModifierToolboxItems> list = (List<ModifierToolboxItems>)actor;
				ModifierToolboxItems selectedItem = list.getSelected();
				if (selectedItem == null) return;
				Modifier modifier = null;
				try {
					modifier = selectedItem.getModifierClass().newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
					throw new IllegalStateException("Unable to create modifier from supplied class type.");
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					throw new IllegalStateException("Unable to create modifier from supplied class type.");
				}
				stackPanel.addModifierBeforeSelected(modifier);
				list.setSelectedIndex(-1);
			}
		});
		final ScrollPane toolboxLowerScrollPane = new ScrollPane(modifierToolbox, skin);
		//insert both into splitpane
		toolboxPane = new SplitPane(toolboxUpperScrollPane, toolboxLowerScrollPane, true, skin);
		
		stage.addActor(artWindow);
		stage.addActor(stackPanel);
		stage.addActor(toolboxPane);
		
		InputAdapter mainInputProcessor = new InputAdapter() {
			boolean	stackDisabled;
			
			@Override
			public boolean keyTyped(char character) {
				switch (character) {
				case 'd':
				case 'D':
					debugMode = debugMode.next();
					return true;
				case 't':
				case 'T':
					stackDisabled = !stackDisabled;
					stackPanel.setStackDisabled(stackDisabled);
					return true;
				default:
					break;
				}
				return false;
			}
			
		};
		InputMultiplexer multiPlexer = new InputMultiplexer(stage, mainInputProcessor);
		Gdx.input.setInputProcessor(multiPlexer);
	}
	
	@Override
	public void render() {
		fps.log();
		Gdx.gl.glClearColor(.25f, .25f, .25f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
		switch (debugMode) {
		case DEBUG_ALL:
			stage.setDebugAll(true);
			stage.setDebugUnderMouse(false);
			stage.setDebugParentUnderMouse(false);
			stage.setDebugTableUnderMouse(false);
			break;
		case DEBUG_UNDER_MOUSE:
			stage.setDebugAll(false);
			stage.setDebugUnderMouse(true);
			stage.setDebugParentUnderMouse(false);
			stage.setDebugTableUnderMouse(false);
			break;
		case DEBUG_PARENT_UNDER_MOUSE:
			stage.setDebugAll(false);
			stage.setDebugUnderMouse(false);
			stage.setDebugParentUnderMouse(true);
			stage.setDebugTableUnderMouse(false);
			break;
		case DEBUG_TABLE_UNDER_MOUSE:
			stage.setDebugAll(false);
			stage.setDebugUnderMouse(false);
			stage.setDebugParentUnderMouse(false);
			stage.setDebugTableUnderMouse(true);
			break;
		default:
			stage.setDebugAll(false);
			stage.setDebugUnderMouse(false);
			stage.setDebugParentUnderMouse(false);
			stage.setDebugTableUnderMouse(false);
			break;
		}
		stackPanel.process();//TODO USURE
		stage.draw();
		if ( activeTool != null )
			activeTool.draw();
	}
	
	@Override
	public void resize(int newWidth, int newHeight) {
		camera.setToOrtho(false, newWidth, newHeight);
		viewport.update(newWidth, newHeight);
		stackPanel.setBounds(newWidth - STACK_WIDTH, 0, STACK_WIDTH, newHeight);
		float toolboxWidth = 100.0f;
		toolboxPane.setBounds(0, 0, toolboxWidth, newHeight);
		artWindow.setBounds(toolboxWidth, 0, newWidth - STACK_WIDTH - toolboxWidth, newHeight);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		Gdx.app.exit();
	}
	
	@Override
	public void vertexAdded(Vertex vertex) {
		if ( activeTool != null )
			vertex.addListener(activeTool.getVertexBehavior());
		/*TODO: remove all tool listeners from all art objects absolutely 
		 * before adding new tool to all art objects. Do this by checking
		 * all of each art object's listeners for instanceof Tool and removing.
		 * OR
		 * do this by making sure all tool listeners are removed upon shape or 
		 * vertex removal and added upon addition. does that call for a 
		 * vertexRemoved app method? if it is to match the add method's approach
		 * then yes.
		 */
	}
	
	@Override
	public ArtWindow getArtWindow() {
		return artWindow;
	}

	private void setActiveTool(Tool tool) {
		if ( tool == null )
			throw new IllegalArgumentException("tool parameter cannot be null");
		Vertex[] vertices = artWindow.getAllVertices();
		if ( activeTool != tool && activeTool != null ) {
			/* remove all tool listeners from stage components */
			artWindow.removeListener(activeTool.getArtWindowBehavior());
			for (Vertex vertex : vertices)
				vertex.removeListener(activeTool.getVertexBehavior());
		}
		activeTool = tool;
		toolboxToolList.setSelected(tool);
		// we can use this for new and for updates because listeners won't add if they've already been added.
		/* set artwindow to respond to this tool's input */
		if ( activeTool.getArtWindowBehavior() != null )
			artWindow.addListener(activeTool.getArtWindowBehavior());
		/* set all vertices to respond to this tool's mouse/key input */
		if ( activeTool.getVertexBehavior() != null )
			for (Vertex vertex : vertices)
				vertex.addListener(activeTool.getVertexBehavior());
	}
	
	@Override
	public void stackPanelProcessBegin() {
		restoreArtWindowBaseState();
	}
	
	@Override
	public void stackPanelProcessEnd() {}

	private void restoreArtWindowBaseState() {
		artWindow.clear();
		for (ShapeState shapeState : baseShapeStates) {
			ShapeState.restore(shapeState);
			artWindow.addActor(shapeState.getHost());
		}
	}
	
	/** created upon flattening. otherwise the basestate is an empty artwindow. */
	private void storeArtWindowAsBaseState() {
		baseShapeStates.clear();
		SnapshotArray<Actor> children = artWindow.getChildren();
		
		Actor[] actors = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Shape shape = (Shape)actors[i];
			baseShapeStates.add(ShapeState.create(shape));
		}
		children.end();
	}
	
	@Override
	public void pause() {}
	
	@Override
	public void resume() {}
	
	enum DebugMode {
		DEBUG_ALL,
		DEBUG_UNDER_MOUSE,
		DEBUG_PARENT_UNDER_MOUSE,
		DEBUG_TABLE_UNDER_MOUSE,
		NO_DEBUG;
		
		private static DebugMode[]	values	= values();
		
		DebugMode next() {
			DebugMode dm = values[(ordinal() + 1) % values.length];
			Gdx.app.log("Debug Mode Set", dm.name());
			return dm;
		}
	}
	
	private Shape	testShape;
}
