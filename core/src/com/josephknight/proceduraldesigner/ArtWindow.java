package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.josephknight.proceduraldesigner.Shape.DrawMode;

public class ArtWindow extends Group {
	private OrthographicCamera		camera;
	private ShapeRenderer			sr;
	public Color					currentFillColor	= Color.WHITE;
	public Color					currentStrokeColor	= Color.BLACK;
	private ArtWindowApplication	app;
	
	public ArtWindow(ArtWindowApplication app) {
		this.app = app;
		this.camera = new OrthographicCamera();
		this.sr = new ShapeRenderer();
		this.sr.setProjectionMatrix(camera.combined);
		
		//TODO verify Group auto resizes to contents
//		this.setSize(initialWidth, initialHeight);
		addListener(new InputListener() {
			//TODO: use or delete. base decision on whether app needs to multiplex certain event types. if so, return false.
			@Override
			public boolean handle(Event e) {
				boolean handled = super.handle(e);
				if ( !(e instanceof InputEvent) )
					return false;
				InputEvent event = (InputEvent)e;
				if ( event.getType() == InputEvent.Type.touchUp ) {
					Gdx.app.log(null, "art window captured touch up and passed not handled to app");
					return false;
				}
				return handled;
			}
		});
	}
	
	public void deselectAll() {
		SnapshotArray<Actor> children = getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Shape shape = (Shape)array[i];
			shape.setVerticesSelected(false);
		}
		children.end();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		SnapshotArray<Actor> children = getChildren();
		Actor[] actors = children.begin();
		final int n = children.size;
		//draw shape fills
		for (int i = 0; i < n; i++) {
			Shape shape = (Shape)actors[i];
			shape.setDrawMode(DrawMode.SHAPE);
		}
		super.draw(batch, parentAlpha);//transforms and draws children
		//draw shape vertices on top
		for (int i = 0; i < n; i++) {
			Shape shape = (Shape)actors[i];
			shape.setDrawMode(DrawMode.VERTICES);
		}
		super.draw(batch, parentAlpha);//transforms and draws children
		children.end();
	}
	
	//Begin: Actor Management *******************************
	@Override
	public void addActor(Actor actor) {
		if ( !(actor instanceof Shape) )
			throw new IllegalArgumentException("ArtWindow only accepts 'Shape's as children.");
		Shape shape = (Shape)actor;
		super.addActor(actor);
		shapeAdded(shape);
	}
	
	@Override
	public void addActorAt(int index, Actor actor) {
		if ( !(actor instanceof Shape) )
			throw new IllegalArgumentException("ArtWindow only accepts 'Shape's as children.");
		super.addActorAt(index, actor);
		shapeAdded((Shape)actor);
	}
	
	@Override
	public void addActorBefore(Actor actorBefore, Actor actor) {
		if ( !(actor instanceof Shape) )
			throw new IllegalArgumentException("ArtWindow only accepts 'Shape's as children.");
		super.addActorBefore(actorBefore, actor);
		shapeAdded((Shape)actor);
	}
	
	@Override
	public void addActorAfter(Actor actorAfter, Actor actor) {
		if ( !(actor instanceof Shape) )
			throw new IllegalArgumentException("ArtWindow only accepts 'Shape's as children.");
		super.addActorAfter(actorAfter, actor);
		shapeAdded((Shape)actor);
	}
	
	//End: Actor Management ***************************************
	
	private void shapeAdded(Shape shape) {
		if ( shape == null )
			throw new NullPointerException("Shape cannot be null");
		shape.setArtWindow(this);
		//Report the new vertices to app
		if ( app != null ) {
			for (Vertex vertex : shape.getVertices()) {
				app.vertexAdded(vertex);
			}
		}
	}
	
	public ArtWindowApplication getArtWindowApplication() {
		return app;
	}
	
	public Vertex[] getAllVertices() {
		Array<Vertex> vertices = new Array<Vertex>();
		
		SnapshotArray<Actor> children = getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			if ( array[i] instanceof Shape ) {
				Shape shape = (Shape)array[i];
				Vertex[] shapeVertices = shape.getVertices();
				vertices.addAll(shapeVertices, 0, shapeVertices.length);				
			}
		}
		children.end();
		
		return vertices.toArray(Vertex.class);
	}
	
	public Vertex[] getSelectedVertices() {
		Vertex[] vertices = getAllVertices();
		Array<Vertex> selection = new Array<Vertex>(vertices.length);
		for (Vertex vertex : vertices) {
			if ( vertex.isSelected() )
				selection.add(vertex);
		}
		return selection.toArray(Vertex.class);
	}
	
	public void setAllVerticesVisible(boolean visible) {
		Vertex[] vertices = getAllVertices();
		for (Vertex vertex : vertices) {
			vertex.setVisible(visible);
		}
	}
	
	@Override
	public void setBounds(float x, float y, float width, float height) {
		super.setBounds(x, y, width, height);
		camera.setToOrtho(false);
		camera.translate(-x, -y);
		camera.update();
		sr.setProjectionMatrix(camera.combined);
	}
	
	public Color getFillColor() {
		return currentFillColor;
	}
	
	public void setCurrentFillColor(Color currentFillColor) {
		this.currentFillColor = currentFillColor;
	}
	
	public Color getStrokeColor() {
		return currentStrokeColor;
	}
	
	public void setCurrentStrokeColor(Color currentStrokeColor) {
		this.currentStrokeColor = currentStrokeColor;
	}
	
	public ShapeRenderer getShapeRenderer() {
		return sr;
	}
}
