package com.josephknight.proceduraldesigner.util;

import com.badlogic.gdx.math.Vector2;
import com.josephknight.proceduraldesigner.Vertex;

public class Util {
	static public Vertex createAveragedVertex(Vertex[] vertices) {
		float sumX = 0, sumY = 0;
		for (Vertex vertex : vertices) {
			sumX += vertex.getX();
			sumY += vertex.getY();
		}
		Vector2 tempVector = new Vector2(sumX / vertices.length, sumY / vertices.length); 
		return new Vertex(tempVector.x, tempVector.y);
	}
}
