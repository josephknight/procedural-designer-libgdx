package com.josephknight.proceduraldesigner.modifiers;

import com.badlogic.gdx.math.Vector2;
import com.josephknight.proceduraldesigner.Modifier;
import com.josephknight.proceduraldesigner.Vertex;
import com.josephknight.proceduraldesigner.parameters.AngleParameter;
import com.josephknight.proceduraldesigner.parameters.IntegerParameter;
import com.josephknight.proceduraldesigner.parameters.RandomNumberParameter;
import com.josephknight.proceduraldesigner.parameters.RangeParameter;


public class TestModifier extends Modifier {
	
	RangeParameter			distance	= new RangeParameter("Distance", this, 0f, 100f);
	AngleParameter			angle		= new AngleParameter("Direction", this);
	RandomNumberParameter	random		= new RandomNumberParameter("Random Number", this);
	Vector2					translation	= new Vector2();
	private static int		count		= 0;
	
	public TestModifier() {
		addParameter(distance);
		addParameter(angle);
		addParameter(random);
		addParameter(new IntegerParameter("Whole Number (Integer)", this));
		setName("Distance Test " + ++count);
	}
	
	@Override
	protected void modifySelection() {
		translation.set(1, 1);
		translation.setLength(distance.getValue());
		translation.setAngle(angle.getValue(AngleParameter.UnitMode.DEGREES));
		for (Vertex vertex : selection) {
			vertex.setPosition(vertex.getX() + translation.x, vertex.getY() + translation.y);
		}
	}
}
