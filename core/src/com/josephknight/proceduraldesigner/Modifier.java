package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.SnapshotArray;

public abstract class Modifier {
	
	protected ObjectSet<Vertex>			selection		= new ObjectSet<Vertex>();
	private SnapshotArray<Parameter>	parameters		= new SnapshotArray<Parameter>(Parameter.class);
	private Array<ShapeState>			shapeBackups	= new Array<ShapeState>(ShapeState.class);
	private String						name			= "unnamed modifier";
	private ModifierChangeListener		modifierChangeListener;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return getName();
	};
	
	public void setSelection(Vertex[] selection) {
		if ( selection == null )
			throw new NullPointerException("New selection cannot be null.");
		this.selection.clear();
		this.selection.addAll(selection);
	}
	
	public void process() {
		modifySelection();
	}
	
	protected abstract void modifySelection();
	
	/** Called by Parameter abstract class when its own core value is changed.
	 * 
	 * @param parameter */
	public void parameterChanged(Parameter parameter) {
		if ( modifierChangeListener != null )
			modifierChangeListener.modifierChanged(this, parameter);
	};
	
	@SuppressWarnings("unused")
	private void backupSelectedShapes() {
		//collect all shapes that have vertices in our selection
		//if a vertex is added or removed from a shape then that shape
		//is affected. therefore we need to backup vertices AND SHAPES.
		Array<Shape> shapesSelected = new Array<Shape>();
		for (Vertex vertex : selection) {
			Shape parentShape = (Shape)vertex.getParent();
			if ( !shapesSelected.contains(parentShape, true) )
				shapesSelected.add(parentShape);
		}
		for (Shape shape : shapesSelected) {
			shapeBackups.add(ShapeState.create(shape));
		}
	}
	
	@SuppressWarnings("unused")
	private void restoreSelectedShapesStates() {
		if ( shapeBackups.size == 0 )
			return;
		for (ShapeState backup : shapeBackups) {
			ShapeState.restore(backup);
		}
	}
	
	protected void addParameter(Parameter parameter) {
		parameters.add(parameter);
	};
	
	public SnapshotArray<Parameter> getParameters() {
		return parameters;
	}
	
	public void setModifierChangeListener(ModifierChangeListener listener) {
		this.modifierChangeListener = listener;
	}
	
	public interface ModifierChangeListener {
		public void modifierChanged(Modifier modifier, Parameter parameter);
	}
	
}
