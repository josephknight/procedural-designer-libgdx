package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import com.josephknight.proceduraldesigner.Parameter;
import com.josephknight.proceduraldesigner.ParameterBar;

public class RandomNumberBar extends ParameterBar {
	
	private TextField	minTextField			= new TextField("unset", skin);
	private TextField	maxTextField			= new TextField("unset", skin);
	private CheckBox	overrideToggle	= new CheckBox("activate", skin, "toggle-round");
	private Label		minValueLabel			= new Label("unset", skin);
	private Label		maxValueLabel			= new Label("unset", skin);
	private Slider		previewValueSlider		= new Slider(0.0f, 1.0f, 0.001f, false, skin);

	//TODO: resize all parambars to prepare for scrollbars
	//TODO: state of usePreviewValue should dim certain components
	//TODO: fix layout glitches
	//TODO: fix non-action
	
	public RandomNumberBar(final RandomNumberParameter parameter) {
		super(parameter);
		
		/*
		 
		 "Random Number"
		 [ 44.3452045184552804109 ]
		 min [ 0 ]		max [ 100 ]
		 
		 ..........................
		 
		 "Override With Preview Value"
		 ( o) activate
		 0						100
		 |--------^---------------|	
		 
		 */
		
		row();
		add(nameLabel);
		
		row();
		add(valueField);
		
		row().expand(false, false);
		Table minMaxTable = new Table();
		minMaxTable.defaults().left().pad(4);
		minMaxTable.add(new Label("min", skin));
		minMaxTable.add(minTextField).width(80);
		minMaxTable.add(new Label("max", skin));
		minMaxTable.add(maxTextField).width(80);
		add(minMaxTable);
		
		//TODO: design panel frame table with inlaid border
		row();
		add(new Label("Override with preview value", skin));
		row();
		add(overrideToggle);
		row();
		Table previewValueSliderTable = new Table();
		previewValueSliderTable.defaults().pad(4).left();
		previewValueSliderTable.row();
		previewValueSliderTable.add(minValueLabel).left();
		previewValueSliderTable.add(maxValueLabel).right();
		previewValueSliderTable.row();
		previewValueSliderTable.add(previewValueSlider).colspan(2).expandX().fillX();
		add(previewValueSliderTable).expandX().fillX();
		
		minTextField.setTextFieldFilter(new TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char c) {
				return Character.isDigit(c) || (c == '.');
			}
		});
		minTextField.setTextFieldListener(new TextField.TextFieldListener() {
			@Override
			public void keyTyped(TextField textField, char c) {
				if ( c == '\r' || c == '\n' )
					parameter.setMin(textFieldToFloat(textField));
			}
		});
		minTextField.addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
				parameter.setMin(textFieldToFloat((TextField)actor));
			}
		});
		maxTextField.setTextFieldFilter(new TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char c) {
				return Character.isDigit(c) || (c == '.');
			}
		});
		maxTextField.setTextFieldListener(new TextField.TextFieldListener() {
			@Override
			public void keyTyped(TextField textField, char c) {
				if ( c == '\r' || c == '\n' )
					parameter.setMax(textFieldToFloat(textField));
			}
		});
		maxTextField.addListener(new FocusListener() {
			@Override
			public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
				parameter.setMax(textFieldToFloat((TextField)actor));
			}
		});
		overrideToggle.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				CheckBox toggle = (CheckBox)actor;
				parameter.setIsOverrideOn(toggle.isChecked());
			}
		});
		previewValueSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Slider preview = (Slider)actor;
				if ( preview.isDragging() ) {
					parameter.setOverrideValue(preview.getValue());
				}
			}
		});
	}
	
	@Override
	public void parameterChanged(Parameter parameter) {
		super.parameterChanged(parameter);
		RandomNumberParameter param = (RandomNumberParameter)parameter;
		minTextField.setText(Float.toString(param.getMin()));
		maxTextField.setText(Float.toString(param.getMax()));
		minValueLabel.setText(Float.toString(param.getMin()));
		maxValueLabel.setText(Float.toString(param.getMax()));
		overrideToggle.setChecked(param.isOverrideOn());
		previewValueSlider.setValue(param.overrideValue);
		previewValueSlider.setRange(param.getMin(), param.getMax());
	}
}
