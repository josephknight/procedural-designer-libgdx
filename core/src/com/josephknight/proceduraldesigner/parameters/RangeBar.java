package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.josephknight.proceduraldesigner.Parameter;
import com.josephknight.proceduraldesigner.ParameterBar;

public class RangeBar extends ParameterBar {
	private Slider	rangeSlider;
	
	public RangeBar(RangeParameter parameterRange) {
		super(parameterRange);
		row();
		add(nameLabel).left().colspan(2);
		row();
		add(valueField).width(60.0f);
		float stepSize = Math.abs(parameterRange.max - parameterRange.min) / 100.0f;
		rangeSlider = new Slider(parameterRange.min, parameterRange.max, stepSize, false, skin);
		rangeSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Slider slider = (Slider)actor;
				if ( slider.isDragging() ){
					Gdx.app.log("ParameterBarRange.rangeSlider change", "updating parameter from slider change.");
					parameter.setValue(slider.getValue());
				}
			}
		});
		add(rangeSlider).expandX().width(180.0f);
	}
	
	@Override
	public void parameterChanged(Parameter parameter) {
		super.parameterChanged(parameter);
		Gdx.app.log("ParameterBarRange.parameterChanged", "setting range slider.");
		rangeSlider.setValue(parameter.getValue());
	}
}
