package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.RandomXS128;
import com.josephknight.proceduraldesigner.Modifier;
import com.josephknight.proceduraldesigner.Parameter;

public class RandomNumberParameter extends Parameter {
	
	float						min	= 0.0f;
	float						max	= 1.0f;
	float						overrideValue;
	boolean						isOverrideOn;
	
	private static RandomXS128	random;
	private static long			seed;
	private static long			stateSeed0;
	private static long			stateSeed1;
	
	public RandomNumberParameter(String name, Modifier parentModifier) {
		this(name, parentModifier, 0.0f, 1.0f);
	}
	
	public RandomNumberParameter(String name, Modifier parentModifier, float min, float max) {
		super(name, parentModifier);
		this.min = min;
		this.max = max;
		if ( random == null )
			random = new RandomXS128();
		setValue(random.nextFloat());
	}
	
	public void next() {
		setValue(random.nextFloat());
	}
	
	@Override
	public float getValue() {
		return isOverrideOn? overrideValue : super.getValue() * (max - min) + min;
	}
	
	public static void setSeed(long seed) {
		random.setSeed(seed);
		RandomNumberParameter.seed = seed;
		saveState();
	}
	
	public static void rewind() {
		random.setSeed(seed);
		saveState();
	}
	
	public static void saveState() {
		stateSeed0 = random.getState(0);
		stateSeed1 = random.getState(1);
	}
	
	public static void restoreState() {
		random.setState(stateSeed0, stateSeed1);
	}
	
	public void setIsOverrideOn(boolean isOverrideOn) {
		this.isOverrideOn = isOverrideOn;
		notifyChange();
	}
	
	public boolean isOverrideOn() {
		return isOverrideOn;
	}
	
	public void setOverrideValue(float value) {
		this.overrideValue = MathUtils.clamp(value, min, max);
		if ( isOverrideOn )
			notifyChange();
	}
	
	public float getMin() {
		return min;
	}
	
	public void setMin(float min) {
		this.min = min;
		MathUtils.clamp(value, min, min);
		notifyChange();
	}
	
	public float getMax() {
		return max;
	}
	
	public void setMax(float max) {
		this.max = max;
		MathUtils.clamp(value, min, max);
		notifyChange();
	}
	
}
