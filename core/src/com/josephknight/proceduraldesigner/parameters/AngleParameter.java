package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.math.MathUtils;
import com.josephknight.proceduraldesigner.Modifier;
import com.josephknight.proceduraldesigner.Parameter;

public class AngleParameter extends Parameter {
	
	private UnitMode	unitMode	= UnitMode.DEGREES;
	
	public AngleParameter(String name, Modifier parentModifier) {
		super(name, parentModifier);
	}
	
	public UnitMode getMode() {
		return unitMode;
	}
	
	public void setMode(UnitMode newMode) {
		float progress = (value - unitMode.min) / (unitMode.max - unitMode.min);
		this.unitMode = newMode;
		setValue(MathUtils.lerp(newMode.min, newMode.max, progress));
	}
	
	public float modulate() {
		float newValue = value % unitMode.max;
		setValue(newValue);
		return newValue;
	}
	
	public float getValue(UnitMode mode) {
		if ( mode == unitMode )
			return getValue();
		switch (mode) {
		case DEGREES:
			if ( unitMode == UnitMode.PERCENTAGE )
				return (getValue() * 360.0f);
			if ( unitMode == UnitMode.RADIANS )
				return (getValue() / 2.0f * 360.0f);
		case PERCENTAGE:
			if ( unitMode == UnitMode.DEGREES )
				return getValue() / 360.0f;
			if ( unitMode == UnitMode.RADIANS )
				return getValue() / 2.0f;
		case RADIANS:
			if ( unitMode == UnitMode.PERCENTAGE )
				return getValue() * 2.0f;
			if ( unitMode == UnitMode.DEGREES )
				return getValue() / 360.0f * 2.0f;
		default:
			return 0.0f;
		}
	}
	
	public enum UnitMode {
		DEGREES (0.0f, 360.0f),
		RADIANS (0.0f, 2.0f),
		PERCENTAGE (0.0f, 1.0f);
		
		private final float	min;
		private final float	max;
		
		UnitMode(float min, float max) {
			this.min = min;
			this.max = max;
		}
		
		public float min() {
			return this.min;
		}
		
		public float max() {
			return this.max;
		}
	}
}
