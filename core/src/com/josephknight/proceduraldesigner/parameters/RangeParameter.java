package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.math.MathUtils;
import com.josephknight.proceduraldesigner.Modifier;
import com.josephknight.proceduraldesigner.Parameter;


public class RangeParameter extends Parameter {
	
	public float	min;
	public float	max;
	
	public RangeParameter(String name, Modifier parentModifier, float min, float max) {
		super(name, parentModifier);
		this.min = min;
		this.max = max;
	}
	
	@Override
	public float getValue() {
		return super.getValue();
	}
	
	@Override
	public void setValue(float value) {
		super.setValue(MathUtils.clamp(value, min, max));
	}
	
	@Override
	public String toString() {
		return name + ": from: " + min + " to: " + max;
	}
}
