package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.josephknight.proceduraldesigner.Parameter;
import com.josephknight.proceduraldesigner.ParameterBar;

public class AngleBar extends ParameterBar {
	private Slider		angleSlider;
	private Label		labelMin;
	private Label		labelMax;
	private TextButton	deg;
	private TextButton	rad;
	private TextButton	pct;
	
	public AngleBar(final AngleParameter parameter) {
		super(parameter);
		//Main Parameter Name label
		row();
		add(nameLabel).left().colspan(2);
		
		row();
		
		//Value field
		add(valueField).width(60.0f);
		
		//Modulate Button
		TextButton modulateButton = new TextButton("Modulate", skin);
		modulateButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.modulate();
			}
		});
		add(modulateButton);
		
		//Unit Mode (button group)
		deg = new TextButton("deg", skin, "toggle");
		rad = new TextButton("rad", skin, "toggle");
		pct = new TextButton("pct", skin, "toggle");
		deg.setProgrammaticChangeEvents(false);
		rad.setProgrammaticChangeEvents(false);
		pct.setProgrammaticChangeEvents(false);
		deg.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(AngleParameter.UnitMode.DEGREES);
			}
		});
		rad.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(AngleParameter.UnitMode.RADIANS);
			}
		});
		pct.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(AngleParameter.UnitMode.PERCENTAGE);
			}
		});
		new ButtonGroup<TextButton>(deg, rad, pct);
		Table modesButtonTable = new Table();
		modesButtonTable.defaults();
		modesButtonTable.add(deg, rad, pct);
		add(modesButtonTable);
		
		row();
		
		//slider min/max labels
		labelMin = new Label("", skin);
		labelMax = new Label("", skin);
		add(labelMin).left();
		add(labelMax).right().colspan(2);
		
		row();
		
		//Slider
//		float stepSize = Math.abs(parameter.getMode().max() - parameter.getMode().min()) / 100.0f;
		float stepSize = 0.01f;
		angleSlider = new Slider(parameter.getMode().min(), parameter.getMode().max(), stepSize, false, skin);
		angleSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Slider slider = (Slider)actor;
				if ( slider.isDragging() ) {
					Gdx.app.log("angleSlider changed", "dragging so set parameter to: " + angleSlider.getValue());
					parameter.setValue(angleSlider.getValue());
				} else {
					Gdx.app.log("angleSlider changed", "ignoring because not dragging.");
				}
			}
		});
		add(angleSlider).expandX().fillX().colspan(3);
	}
	
	@Override
	public void parameterChanged(Parameter parameter) {
		super.parameterChanged(parameter);
		AngleParameter parameterAngle = (AngleParameter)parameter;
		Gdx.app.log("PBAngle.parameterChanged", "setting slider to: " + parameter.getValue());
		float min = parameterAngle.getMode().min();
		float max = parameterAngle.getMode().max();
		angleSlider.setRange(min, max);
		angleSlider.setValue(parameter.getValue());
		labelMin.setText(Float.toString(min));
		labelMax.setText(Float.toString(max));
		switch (parameterAngle.getMode()) {
		case DEGREES:
			deg.setChecked(true);
			break;
		case RADIANS:
			rad.setChecked(true);
			break;
		case PERCENTAGE:
			pct.setChecked(true);
			break;
		default:
			break;
		}
	}
}
