package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.math.MathUtils;
import com.josephknight.proceduraldesigner.Modifier;
import com.josephknight.proceduraldesigner.Parameter;

public class IntegerParameter extends Parameter {
	
	private RoundingMode	mode	= RoundingMode.DOWN;
	
	public IntegerParameter(String name, Modifier parentModifier) {
		super(name, parentModifier);
	}
	
	@Override
	public float getValue() {
		switch (mode) {
		case DOWN:
			return MathUtils.floor(value);
		case NORMAL:
			return MathUtils.round(value);
		case UP:
			return MathUtils.ceil(value);
		default:
			throw new RuntimeException("Integer parameter's mode must be set before attempting to get value.");
		}
	}
	
	public RoundingMode getMode() {
		return mode;
	}
	
	public void setMode(RoundingMode mode) {
		this.mode = mode;
		notifyChange();
	}
	
	/** Round always down, normally (<.5 = down, >.5 = up), or always up. */
	public enum RoundingMode {
		DOWN,
		NORMAL,
		UP;
	}
}
