package com.josephknight.proceduraldesigner.parameters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.josephknight.proceduraldesigner.Parameter;
import com.josephknight.proceduraldesigner.ParameterBar;

public class IntegerBar extends ParameterBar {
	
	private TextButton	roundDown;
	private TextButton	roundNormal;
	private TextButton	roundUp;
	private Label		labelMin;
	private Label		labelMax;
	private Slider		valueSlider;
	
	public IntegerBar(final IntegerParameter parameter) {
		super(parameter);
		//Main Parameter Name label
		row();
		add(nameLabel).left().colspan(2);
		
		row();
		
		//Value field
		add(valueField).width(60.0f);
		
		//Unit Mode (button group)
		roundDown = new TextButton("Down", skin, "toggle");
		roundNormal = new TextButton("Normal", skin, "toggle");
		roundUp = new TextButton("Up", skin, "toggle");
		roundDown.setProgrammaticChangeEvents(false);
		roundNormal.setProgrammaticChangeEvents(false);
		roundUp.setProgrammaticChangeEvents(false);
		roundDown.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(IntegerParameter.RoundingMode.DOWN);
			}
		});
		roundNormal.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(IntegerParameter.RoundingMode.NORMAL);
			}
		});
		roundUp.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				parameter.setMode(IntegerParameter.RoundingMode.UP);
			}
		});
		new ButtonGroup<TextButton>(roundDown, roundNormal, roundUp);
		Table modesButtonTable = new Table();
		modesButtonTable.defaults();
		modesButtonTable.add(roundDown, roundNormal, roundUp);
		add(modesButtonTable);
		
		row();
		
		//slider min/max fields
		labelMin = new Label("", skin);
		labelMax = new Label("", skin);
		add(labelMin).left();
		add(labelMax).right().colspan(2);
		
		row();
		
		//Slider
		float stepSize = 1f;//always
		valueSlider = new Slider(0, 100, stepSize, false, skin);
		valueSlider.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Slider slider = (Slider)actor;
				if ( slider.isDragging() ) {
					Gdx.app.log("IntegerBar:valueSlider changed", "dragging so set parameter to: " + valueSlider.getValue());
					parameter.setValue(valueSlider.getValue());
				} else {
					Gdx.app.log("IntegerBar:valueSlider changed", "ignoring because not dragging.");
				}
			}
		});
		add(valueSlider).expandX().fillX().colspan(3);
	}
	
	@Override
	public void parameterChanged(Parameter parameter) {
		Gdx.app.log("IntegerBar.parameterChanged", "setting slider to: " + parameter.getValue());
		super.parameterChanged(parameter);
		IntegerParameter intParam = (IntegerParameter)parameter;
		valueSlider.setValue(parameter.getValue());
		switch (intParam.getMode()) {
		case DOWN:
			roundDown.setChecked(true);
			break;
		case NORMAL:
			roundNormal.setChecked(true);
			break;
		case UP:
			roundUp.setChecked(true);
			break;
		default:
			break;
		}
	}
	
}
