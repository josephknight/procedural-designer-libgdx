package com.josephknight.proceduraldesigner;

import java.security.InvalidParameterException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Disableable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.SnapshotArray;

public class ModifierBar extends Table implements Disableable, Poolable {
	
	//Core fields
	private Modifier			modifier;
	private StackPanel			stackPanel;
	private boolean				isSelected;
	private boolean				isDisabled;
	private boolean				isProcessingDisabled;
	//UI fields
	private CheckBox			processingEnabledToggleButton;
	private Label				nameLabel;
	private Array<ParameterBar>	parameterBars	= new Array<ParameterBar>(ParameterBar.class);
	private ImageButton			detent;
	
	public ModifierBar() {
		super();
		this.setName("");
		this.setTouchable(Touchable.enabled);
	}
	
	public void initialize(Modifier modifier, Skin skin) {
		if ( modifier == null )
			throw new InvalidParameterException("modifier must not be null.");
		if ( skin == null )
			throw new InvalidParameterException("skin must not be null.");
		if ( processingEnabledToggleButton == null )
			processingEnabledToggleButton = new CheckBox("", skin, "toggle-round");
		if ( nameLabel == null )
			nameLabel = new Label("undefined modifier", skin, "colorable");
		if ( detent == null )
			detent = new ImageButton(skin, "detent");
		this.setSkin(skin);
		processingEnabledToggleButton.setSkin(skin);
		nameLabel.setTouchable(Touchable.disabled);
		nameLabel.setEllipsis(true);
		detent.setSkin(skin);
		processingEnabledToggleButton.setChecked(true);
		processingEnabledToggleButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				CheckBox toggle = (CheckBox)actor;
				if ( toggle.isChecked() )
					setProcessingDisabled(false);
				else
					setProcessingDisabled(true);
			}
		});
		detent.setTouchable(Touchable.disabled);
		this.background(skin.getDrawable("panel-bg"));
		this.add(processingEnabledToggleButton).left().padLeft(8).padRight(8);
		this.add(nameLabel).expand().left();
		this.add(detent).width(20).pad(16);
		this.modifier = modifier;
		this.setName(modifier.getName());
		nameLabel.setText(modifier.getName());
		//create visual components from the core modifier's parameters
		SnapshotArray<Parameter> parametersSnapshot = modifier.getParameters();
		Parameter[] parameters = parametersSnapshot.begin();
		for (int i = 0, n = parametersSnapshot.size; i < n; i++) {
			Parameter parameter = parameters[i];
			ParameterBar parameterBar = BarFactory.createParameterBar(parameter);
			//setup each parambar's initial details
			parameterBar.parameter.notifyChange();
			parameterBars.add(parameterBar);
		}
		parametersSnapshot.end();
	}
	
	public void setStackPanel(StackPanel stackPanel) {
		this.stackPanel = stackPanel;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		Color fontColor;
		//selection
		if ( isSelected ) {
			setBackground(getSkin().getDrawable("panel-bg-selected"));
			if ( isDisabled )
				fontColor = getSkin().getColor("text-disabled-selected");
			else
				fontColor = getSkin().getColor("text-normal-selected");
		} else {
			setBackground(getSkin().getDrawable("panel-bg"));
			if ( isDisabled )
				fontColor = getSkin().getColor("text-disabled");
			else
				fontColor = getSkin().getColor("text-normal");
		}
//		processing toggle
		if ( isProcessingDisabled ) {
			nameLabel.setColor(fontColor.r, fontColor.g, fontColor.b, fontColor.a * .5f * parentAlpha);
		} else
			nameLabel.setColor(fontColor);
		super.draw(batch, parentAlpha);
	}
	
	@Override
	public boolean isDisabled() {
		return isDisabled;
	}
	
	@Override
	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
		//processing toggle button
		processingEnabledToggleButton.setDisabled(isDisabled);
		if ( stackPanel != null )
			stackPanel.requestProcessing();
	}
	
	public boolean isProcessingDisabled() {
		return isProcessingDisabled;
	}
	
	/** Not to be confused with setDisabled which affects the Actor as a whole,
	 * setProcessingDisabled simply affects whether its modifier is processed
	 * in the stack. User-manipulation of the actor and it's children is not
	 * affected.
	 * 
	 * @param isProcessingDisabled */
	public void setProcessingDisabled(boolean isProcessingDisabled) {
		if ( isProcessingDisabled == this.isProcessingDisabled )
			return;//no infinite loops plz
		this.isProcessingDisabled = isProcessingDisabled;
		this.processingEnabledToggleButton.setChecked(!isProcessingDisabled);
		if ( stackPanel != null )
			stackPanel.requestProcessing();
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	
	public void setSelected(boolean selected) {
		this.isSelected = selected;
	}
	
	public Modifier getModifier() {
		return modifier;
	}
	
	@Override
	public void layout() {
		super.layout();
	}
	
	@Override
	public float getMinWidth() {
		return 200f;
	}
	
	@Override
	public float getMinHeight() {
		return 20f;
	}
	
	@Override
	public float getPrefWidth() {
		return 200f;
	}
	
	@Override
	public float getPrefHeight() {
		return 30.0f;
	}
	
	public Array<ParameterBar> getParameterBars() {
		return parameterBars;
	}
	
	/** Note this is overriding reset() from Poolable, not from Table. Do not
	 * attempt to reset the ModifierBar table through this method. **/
	@Override
	public void reset() {
		this.clear();
		modifier = null;
		stackPanel = null;
		setSelected(false);
		setDisabled(false);
		setProcessingDisabled(false);
		nameLabel.setText("");
		nameLabel.setTouchable(Touchable.disabled);
		parameterBars.clear();
		Gdx.app.log("reset", "ModifierBar");
	}
}
