package com.josephknight.proceduraldesigner;

import com.josephknight.proceduraldesigner.modifiers.TestModifier;

public enum ModifierToolboxItems {
	Test ("Test", TestModifier.class);
	
	final String	toolboxName;
	private final Class<? extends Modifier>	modifierClass;
	
	@SuppressWarnings("unchecked")
	ModifierToolboxItems(String toolboxName, Class<?> modifierClass) {
		this.toolboxName = toolboxName;
		this.modifierClass = (Class<? extends Modifier>)modifierClass;
	}
	
	public String toString() {
		return toolboxName;
	}
	
	public Class<? extends Modifier> getModifierClass(){
		return modifierClass;
	}
}
