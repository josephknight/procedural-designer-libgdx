package com.josephknight.proceduraldesigner;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.SnapshotArray;

//TODO: capture clicks on child vertices and check to see if the vertex 
//is selected, if so, set all this shape's vertices to visible.

//TODO: make sure child vertices can be made visible/invis upon 
//entering/exiting regardless of visible or enabled status.

public class Shape extends Group implements Poolable {
	private ArtWindow		artWindow;
	protected Polygon		polygon		= new Polygon();
	Color					strokeColor	= new Color(0x000000FF);
	float					strokeWidth	= 0;
	private ShapeRenderer	sr;
	private DrawMode		drawMode	= DrawMode.SHAPE;
	Vector2					tmp			= new Vector2();
	
	public Shape() {
		this(Color.GRAY);
	}
	
	public Shape(float[] vertexFloats, Color fillColor) {
		this(fillColor);
		if ( vertexFloats.length % 2 != 0 )
			throw new IllegalArgumentException("vertexFloats must be even x,y pairs");
		for (int i = 0; i < vertexFloats.length; i += 2) {
			Vertex vertex = Pools.obtain(Vertex.class);
			vertex.setPosition(vertexFloats[i], vertexFloats[i + 1]);
			addActor(vertex);
		}
	}
	
	public Shape(Vertex[] vertices, Color fillColor) {
		this(fillColor);
		for (Vertex vertex : vertices) {
			addActor(vertex);
		}
	}
	
	public Shape(Color fillColor) {
		setColor(fillColor);
	}
	
	public void setVertices(float[] vertexFloats) {
		if ( vertexFloats.length % 2 != 0 )
			throw new IllegalArgumentException("vertexFloats must be even x,y pairs");
		polygon.setVertices(vertexFloats);
		SnapshotArray<Actor> children = getChildren();
		//Clear current vertices
		Actor[] actors = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Vertex vertex = (Vertex)actors[i];
			vertex.remove();
			Pools.free(vertex);
		}
		//Obtain proper number of new ones from pool, setting pos
		for (int i = 0; i < vertexFloats.length; i += 2) {
			Vertex newVertex = Pools.obtain(Vertex.class);
			newVertex.setPosition(vertexFloats[i], vertexFloats[i + 1]);
		}
		children.end();
	}
	
	@Override
	public void reset() {
		artWindow = null;
		getColor().set(Color.GRAY);
		polygon.setVertices(new float[] { 0, 0, 0, 0, 0, 0 });
		strokeColor.set(Color.WHITE);
		strokeWidth = 0;
		sr = null;
		drawMode = DrawMode.SHAPE;
		SnapshotArray<Actor> children = getChildren();
		Actor[] actors = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Vertex vertex = (Vertex)actors[i];
			vertex.remove();
			Pools.free(vertex);
		}
		children.end();
		clear();
	}
	
	/** Draw the filled and/or stroked shapes and any highligting outlines such
	 * as
	 * hovering or selected outlines. Happens after draw() when called by
	 * ArtWindow.
	 * 
	 * @param batch
	 * @param sr
	 * @param parentAlpha */
	public void draw(Batch batch, float parentAlpha) {
		if ( !isVisible() )
			return;
		//if not at least a triangle, don't draw it.
		SnapshotArray<Actor> children = getChildren();
		if ( drawMode == DrawMode.VERTICES ) {
			super.draw(batch, parentAlpha);
			return;
		}
		Actor[] actors = children.begin();
		int childrenSize = children.size;
		batch.end();//OPTIMIZE placement of batch end/begin
		//if 3+ vertices then we can draw the shape
		if ( childrenSize >= 3 ) {
			//OPTIMIZE update polygon only if needed. separate polygon update code.
			float[] vertexFloats = new float[childrenSize * 2];
			for (int i = 0; i < childrenSize; i++) {
				//sometimes .begin() returns an array bigger than it's contents.
				if ( actors[i] == null )
					break;
				Vertex vertex = (Vertex)actors[i];
				vertexFloats[i * 2] = vertex.getX();
				vertexFloats[i * 2 + 1] = vertex.getY();
			}
			polygon.setVertices(vertexFloats);
			//end optimize *****************
			sr.begin(ShapeType.Filled);
			Color fillColor = this.getColor();
			sr.setColor(fillColor.r, fillColor.g, fillColor.b, parentAlpha * fillColor.a);
			sr.polygon(polygon.getVertices());
			sr.end();
		}
		//stroke
		if ( strokeWidth > 0 ) {
			sr.begin(ShapeType.Filled);
			sr.setColor(strokeColor);
			for (int i = 0; i < childrenSize; i++) {
				Vertex v = (Vertex)actors[i];
				//if we're on the last vertex then the "next" on is the first one, otherwise just use the next one.
				Vertex vNext = (i + 1 == childrenSize)? (Vertex)actors[0] : (Vertex)actors[i + 1];
				//draw a line between each of proper strokeWidth thickness.
				sr.rectLine(v.getX(), v.getY(), vNext.getX(), vNext.getY(), strokeWidth);
			}
			sr.end();
		}
		//if some of the vertices are selected draw a selection line between
		//any that are adjacent to represent their segment as also virtually selected.
		Vertex[] selectedVertices = getSelectedVertices();
		if ( selectedVertices.length > 0 ) {
			sr.begin(ShapeType.Filled);
			sr.setColor(Color.BLUE);
			for (int i = 0, n = childrenSize; i < n; i++) {
				Vertex v = (Vertex)actors[i];
				//if we're on the last vertex then the "next" on is the first one, otherwise just use the next one.
				Vertex vNext = (i + 1 == childrenSize)? (Vertex)actors[0] : (Vertex)actors[i + 1];
				//draw a special thin "selected" line between these two only if they are both selected
				if ( v.isSelected() && vNext.isSelected() )
					sr.rectLine(v.getX(), v.getY(), vNext.getX(), vNext.getY(), 1.0f);
			}
			sr.end();
		}
		children.end();
		batch.begin();
	}
	
	@Override
	protected void drawChildren(Batch batch, float parentAlpha) {
		if ( drawMode == DrawMode.VERTICES )
			super.drawChildren(batch, parentAlpha);
	}
	
	@Override
	public Actor hit(float x, float y, boolean touchable) {
		//TODO: is any of this necessary?
		if ( touchable && getTouchable() == Touchable.disabled )
			return null;
		Vector2 point = tmp;
		//if the point lies on a vertex, return that.
		SnapshotArray<Actor> children = getChildren();
		Actor[] array = children.begin();
		for (int i = children.size - 1; i >= 0; i--) {
			Actor child = array[i];
			if ( !child.isVisible() )
				continue;
			child.parentToLocalCoordinates(point.set(x, y));
			Actor hit = child.hit(point.x, point.y, touchable);
			if ( hit != null )
				return hit;
		}
		children.end();
		point.set(x, y);
		if ( polygon.contains(point.x, point.y) )
			return this;
		else
			return null;
	}
	
	public Vertex[] getVertices() {
		SnapshotArray<Actor> actors = getChildren();
		return actors.toArray(Vertex.class);
	}
	
	public Vertex[] getSelectedVertices() {
		SnapshotArray<Actor> childrenArray = getChildren();
		Actor[] children = childrenArray.begin();
		Array<Vertex> selectedVertices = new Array<Vertex>(childrenArray.size);
		for (int i = 0, n = childrenArray.size; i < n; i++) {
			Vertex vertex = (Vertex)children[i];
			if ( vertex.isSelected() )
				selectedVertices.add(vertex);
		}
		childrenArray.end();
		return selectedVertices.toArray(Vertex.class);
	}
	
	public Color getStrokeColor() {
		return strokeColor;
	}
	
	public void setStrokeColor(Color strokeColor) {
		this.strokeColor = strokeColor;
	}
	
	public float getStrokeWidth() {
		return strokeWidth;
	}
	
	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
	}
	
	public void translate(float x, float y) {
		polygon.translate(x, y);
	}
	
	public void rotate(float degrees) {
		polygon.rotate(degrees);
	}
	
	public void scale(float amount) {
		polygon.scale(amount);
	}
	
	public float getOriginX() {
		return polygon.getOriginX();
	}
	
	public float getOriginY() {
		return polygon.getOriginY();
	}
	
	public void setOrigin(float originX, float originY) {
		polygon.setOrigin(originX, originY);
	}
	
	public void setPosition(float x, float y) {
		polygon.setPosition(x, y);
	}
	
	@Override
	public void addActor(Actor actor) {
		if ( !(actor instanceof Vertex) )
			throw new IllegalArgumentException("Shapes only select child actors of type Vertex");
		super.addActor(actor);
		vertexAdded((Vertex)actor);
	}
	
	
	@Override
	public void addActorAt(int index, Actor actor) {
		if ( !(actor instanceof Vertex) )
			throw new IllegalArgumentException("Shapes only select child actors of type Vertex");
		super.addActorAt(index, actor);
		vertexAdded((Vertex)actor);
	}
	
	@Override
	public void addActorBefore(Actor actorBefore, Actor actor) {
		if ( !(actor instanceof Vertex) )
			throw new IllegalArgumentException("Shapes only select child actors of type Vertex");
		super.addActorBefore(actorBefore, actor);
		vertexAdded((Vertex)actor);
	}
	
	@Override
	public void addActorAfter(Actor actorAfter, Actor actor) {
		if ( !(actor instanceof Vertex) )
			throw new IllegalArgumentException("Shapes only select child actors of type Vertex");
		super.addActorAfter(actorAfter, actor);
		vertexAdded((Vertex)actor);
	}
	
	private void vertexAdded(Vertex vertex) {
		ArtWindowApplication app = (artWindow != null)? artWindow.getArtWindowApplication() : null;
		if ( app != null )
			app.vertexAdded(vertex);
	}
	
	public void setArtWindow(ArtWindow artWindow) {
		this.artWindow = artWindow;
		if ( artWindow != null )
			this.sr = artWindow.getShapeRenderer();
	}
	
	public ArtWindow getArtWindow() {
		return artWindow;
	}
	
	@Override
	protected void setParent(Group parent) {
		if ( parent != null && !(parent instanceof ArtWindow) )
			throw new IllegalArgumentException("Shapes can only be added to an ArtWindow");
		super.setParent(parent);
		setArtWindow((ArtWindow)parent);
	}
	
	public DrawMode getDrawMode() {
		return drawMode;
	}
	
	public void setDrawMode(DrawMode drawMode) {
		this.drawMode = drawMode;
	}
	
	public void setVerticesSelected(boolean selected) {
		SnapshotArray<Actor> children = getChildren();
		Actor[] array = children.begin();
		for (int i = 0, n = children.size; i < n; i++) {
			Vertex vertex = (Vertex)array[i];
			vertex.setSelected(selected);
		}
	}
	
	enum DrawMode {
		SHAPE,
		VERTICES
	};
}
